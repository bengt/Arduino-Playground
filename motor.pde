// Motorsachen
int ledPin2 = 5;                  // LogikPin Spule1 linker Motor
int ledPin1 = 6;                  // LogikPin Spule2 linker Motor
int enabelPin = 8;                // Logikpin Motortreiber (der andere wird per Hardware gebrückt)
int ledPin3 = 9;                  // LogikPin Spule1 rechter Motor
int ledPin4 = 10;                 // LogikPin Spule2 rechter Motor

// Tastersachen
int linkerTasterVar = 0;           // Variable für Input-Pegel vom linken Taster
int rechterTasterVar = 0;          // Variable für Input-Pegel vom rechten Taster
int linkerTasterPin = A0;          // Pin Analoger Eingang 0
int rechterTasterPin = A1;         // Pin Analoger Eingang 1
int time;                          // für Tast-Sensoren (Sollen nur alle 100ms abgefragt werden)

// Delay für Testprogramm
int Anzeit = 2000;                 // in millisekunden

void setup(){
Serial.begin(9600);                // für PC - Arduino Kommunikation
pinMode(ledPin1, OUTPUT);          // Motor Output-Pins
pinMode(ledPin2, OUTPUT);
pinMode(ledPin3, OUTPUT);
pinMode(ledPin4, OUTPUT);
pinMode(enabelPin, OUTPUT);        // Motortreiber-Steuerung (Logik vom L293D An / Aus)
}

void loop(){
Vorwaerts();
FrageTasterAb();
}

void Rueckwaerts()
{
digitalWrite(enabelPin, HIGH);
digitalWrite(ledPin1, HIGH);
digitalWrite(ledPin2, LOW);
digitalWrite(ledPin3, HIGH);
digitalWrite(ledPin4, LOW);
}

void Vorwaerts()
{
digitalWrite(enabelPin, HIGH);
digitalWrite(ledPin1, LOW);
digitalWrite(ledPin2, HIGH);
digitalWrite(ledPin3, LOW);
digitalWrite(ledPin4, HIGH);
}

void DreheLinks()
{
digitalWrite(enabelPin, HIGH);
digitalWrite(ledPin1, LOW);
digitalWrite(ledPin2, HIGH);
digitalWrite(ledPin3, HIGH);
digitalWrite(ledPin4, LOW);
}

void DreheRechts(){
digitalWrite(enabelPin, HIGH);
digitalWrite(ledPin1, HIGH);
digitalWrite(ledPin2, LOW);
digitalWrite(ledPin3, LOW);
digitalWrite(ledPin4, HIGH);
}

void Bremse(){                      // Motor bremst/blockiert; hält ein elektr. Feld Aufrecht
digitalWrite(enabelPin, HIGH);
digitalWrite(ledPin1, HIGH);
digitalWrite(ledPin2, HIGH);
digitalWrite(ledPin3, HIGH);
digitalWrite(ledPin4, HIGH);
}

void Auslaufen(){                    // Motor ist im Leerlauf; kein elektr. Feld
  digitalWrite(ledPin1, LOW);
  digitalWrite(ledPin2, LOW);
  digitalWrite(ledPin3, LOW);
  digitalWrite(ledPin4, LOW);
  digitalWrite(enabelPin, LOW);
}

void FrageTasterAb(){
  linkerTasterVar = analogRead(linkerTasterPin);            // messung linker Taster
  rechterTasterVar = analogRead(rechterTasterPin);          // messung rechter Taster
  time = millis()%100;                                      // time wird später abgefragt, so wird nur jede 100te millisekunde gemessen
  if(time == 0){
     switch (linkerTasterVar == 1023 && rechterTasterVar == 1023){          // 
           case 1: Serial.println("beide");                                 // beide sind gedrückt -> zurücksetzen und umdrehen
               Bremse();                                                    // ist besser für Motoren und das Delay von "void piepe()" wird beachtet
               piepe();
               Rueckwaerts();
               delay(Anzeit);
               DreheRechts();
               delay(Anzeit); delay(Anzeit); delay(Anzeit);
               break;
           case 0: if (linkerTasterVar == 1023){
                      Serial.println("links");                              // LINKS ist gedrückt -> zurücksetzen und nach rechts drehen
                      Rueckwaerts();
                      delay(Anzeit);
                      DreheRechts();
                      delay(Anzeit);
                   }
                   else if (rechterTasterVar == 1023) {                     // RECHTS ist gedrückt -> zurücksetzen und nach links drehen
                      Serial.println("rechts");
                      Rueckwaerts();
                      delay(Anzeit);
                      DreheRechts();
                      delay(Anzeit);
                   }
     }
  }
}

void piepe(){
  tone(11, 4500, 100);
  delay(100);
  tone(11, 4500, 100);
}
